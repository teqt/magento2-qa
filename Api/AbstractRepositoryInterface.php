<?php

namespace Teqt\QuestionsAnswers\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

interface AbstractRepositoryInterface
{
    /**
     * @param int $id
     * @return AbstractModel
     */
    public function findById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return AbstractCollection
     */
    public function findAll(SearchCriteriaInterface $criteria);
}