<?php

namespace Teqt\QuestionsAnswers\Api;

use Teqt\QuestionsAnswers\Api\Data\AnswerInterface;

interface AnswerRepositoryInterface extends AbstractRepositoryInterface
{
    /**
     * @param AnswerInterface $answer
     * @throws \Exception
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(AnswerInterface $answer);
}