<?php

namespace Teqt\QuestionsAnswers\Api\Data;

/**
 * Interface AnswerInterface
 *
 * @method id getId() Getter for answer ID
 * @method string getAnswer() Getter for actual answer
 * @method $this setAnswer(string $answer) Setter for actual answer
 * @package Teqt\QuestionsAnswers\Model
 */
interface AnswerInterface
{
    /**
     * @return GuestInterface
     */
    public function getGuest();

    /**
     * @param GuestInterface $guest
     * @return $this
     */
    public function setGuest(GuestInterface $guest);

    /**
     * @return QuestionInterface
     */
    public function getQuestion();

    /**
     * @param QuestionInterface $question
     * @return $this
     */
    public function setQuestion(QuestionInterface $question);
}