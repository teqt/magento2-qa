<?php

namespace Teqt\QuestionsAnswers\Api\Data;

use Teqt\QuestionsAnswers\Model\Resource\Question\Collection as QuestionCollection;
use Teqt\QuestionsAnswers\Model\Resource\Answer\Collection as AnswerCollection;

/**
 * Interface GuestInterface
 *
 * @method id getId() Getter for guest ID
 * @method string getSlug() Getter for slug
 * @method $this setSlug(string $slug) Setter for slug
 * @method string getDisplayName() Getter for display name
 * @method $this setDisplayName(string $display_name) Setter for display name
 * @method string getEmail() Getter for email
 * @method $this setEmail(string $email) Setter for email
 * @package Teqt\QuestionsAnswers\Model
 */
interface GuestInterface
{
    /**
     * @param string $displayField
     * @return string
     */
    public function getLink($displayField);

    /**
     * @return QuestionCollection
     */
    public function getQuestions();

    /**
     * @return AnswerCollection
     */
    public function getAnswers();
}