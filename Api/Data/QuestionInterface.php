<?php

namespace Teqt\QuestionsAnswers\Api\Data;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Interface QuestionInterface
 *
 * @method id getId() Getter for question ID
 * @method string getQuestion() Getter for actual question
 * @method $this setQuestion(string $question) Setter for actual question
 * @package Teqt\QuestionsAnswers\Model
 */
interface QuestionInterface
{
    /**
     * @return GuestInterface
     */
    public function getGuest();

    /**
     * @param GuestInterface $guest
     * @return $this
     */
    public function setGuest(GuestInterface $guest);

    /**
     * @return ProductInterface
     */
    public function getProduct();

    /**
     * @param ProductInterface $product
     * @return $this
     */
    public function setProduct(ProductInterface $product);

    /**
     * @return AnswerCollection|void
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function getAnswers();
}