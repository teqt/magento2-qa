<?php

namespace Teqt\QuestionsAnswers\Api;

use Teqt\QuestionsAnswers\Api\Data\GuestInterface;

interface GuestRepositoryInterface extends AbstractRepositoryInterface
{
    /**
     * @param string $slug
     * @return GuestInterface
     */
    public function findBySlug($slug);

    /**
     * @param string $email
     * @return GuestInterface
     */
    public function findByEmail($email);

    /**
     * @return GuestInterface
     */
    public function findByRequest();

    /**
     * @param GuestInterface $question
     * @throws \Exception
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(GuestInterface $question);
}