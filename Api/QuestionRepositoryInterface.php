<?php

namespace Teqt\QuestionsAnswers\Api;

use Magento\Catalog\Api\Data\ProductInterface;
use Teqt\QuestionsAnswers\Api\Data\QuestionInterface;
use Teqt\QuestionsAnswers\Model\Resource\Question\Collection;

interface QuestionRepositoryInterface extends AbstractRepositoryInterface
{
    /**
     * @param ProductInterface $product
     * @return Collection
     */
    public function findByProduct($product);

    /**
     * @param QuestionInterface $question
     * @throws \Exception
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(QuestionInterface $question);
}