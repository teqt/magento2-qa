<?php

namespace Teqt\QuestionsAnswers\Block;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\View\Element\Template;

class AbstractBlock extends Template
{
    /**
     * @param ProductInterface $product
     * @return string
     */
    public function getProductLink(ProductInterface $product)
    {
        return sprintf('<a href="%s#questions">%s</a>',
            $product->getProductUrl(), $product->getName());
    }
}