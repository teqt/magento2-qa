<?php

namespace Teqt\QuestionsAnswers\Block\Answer;

use Teqt\QuestionsAnswers\Api\Data\QuestionInterface;
use Teqt\QuestionsAnswers\Block\AbstractBlock;

class Form extends AbstractBlock
{
    /**
     * @var QuestionInterface
     */
    protected $question;

    /**
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('teqt_qa/answer/submit');
    }

    /**
     * @return QuestionInterface
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param QuestionInterface $question
     * @return $this
     */
    public function setQuestion(QuestionInterface $question)
    {
        $this->question = $question;
        return $this;
    }
}