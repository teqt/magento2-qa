<?php

namespace Teqt\QuestionsAnswers\Block\Guest;

use Teqt\QuestionsAnswers\Block\AbstractBlock;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Api\GuestRepositoryInterface;
use Magento\Framework\View\Element\Template;

class Answers extends AbstractBlock
{
    /**
     * @var GuestInterface
     */
    protected $guest;

    /**
     * Guest constructor.
     * @param Template\Context $context
     * @param GuestRepositoryInterface $guestRepository
     * @param array $data
     */
    public function __construct(Template\Context $context, GuestRepositoryInterface $guestRepository, array $data = [])
    {
        $this->guest = $guestRepository->findByRequest();
        parent::__construct($context, $data);
    }

    /**
     * @return \Teqt\QuestionsAnswers\Model\Resource\Answer\Collection
     */
    public function getAnswers()
    {
        return $this->guest->getAnswers();
    }
}