<?php

namespace Teqt\QuestionsAnswers\Block\Guest;

use Teqt\QuestionsAnswers\Block\AbstractBlock;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Api\GuestRepositoryInterface;
use Magento\Framework\View\Element\Template;

class Questions extends AbstractBlock
{
    /**
     * @var GuestInterface
     */
    protected $guest;

    /**
     * Guest constructor.
     * @param Template\Context $context
     * @param GuestRepositoryInterface $guestRepository
     * @param array $data
     */
    public function __construct(Template\Context $context, GuestRepositoryInterface $guestRepository, array $data = [])
    {
        $this->guest = $guestRepository->findByRequest();
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTitle()
    {
        return __('Questions from this contributor');
    }

    /**
     * @return \Teqt\QuestionsAnswers\Model\Resource\Question\Collection
     */
    public function getQuestions()
    {
        return $this->guest->getQuestions();
    }
}