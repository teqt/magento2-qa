<?php

namespace Teqt\QuestionsAnswers\Block\Guest;

use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Api\GuestRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Teqt\QuestionsAnswers\Block\AbstractBlock;

class View extends AbstractBlock
{
    /**
     * @var GuestInterface
     */
    protected $guest;

    /**
     * Guest constructor.
     * @param Template\Context $context
     * @param GuestRepositoryInterface $guestRepository
     * @param array $data
     */
    public function __construct(Template\Context $context, GuestRepositoryInterface $guestRepository, array $data = [])
    {
        $this->guest = $guestRepository->findByRequest();
        parent::__construct($context, $data);
    }

    /**
     * @return GuestInterface
     */
    public function getGuest()
    {
        return $this->guest;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTitle()
    {
        return __('Contributions by %1', $this->getGuest()->getDisplayName());
    }

    /**
     * @return $this
     */
    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set($this->getTitle());
        return parent::_prepareLayout();
    }
}