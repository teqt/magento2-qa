<?php

namespace Teqt\QuestionsAnswers\Block\Question;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Teqt\QuestionsAnswers\Block\AbstractBlock;

class Form extends AbstractBlock
{
    /**
     * @var ProductInterface
     */
    protected $product;

    /**
     * Questions constructor.
     * @param Template\Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param array $data
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function __construct(
        Template\Context $context,
        ProductRepositoryInterface $productRepository,
        array $data = []
    )
    {
        $productId = $context->getRequest()->getParam('id');
        $this->product = $productRepository->getById($productId);

        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('teqt_qa/question/submit');
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }
}