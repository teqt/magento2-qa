<?php

namespace Teqt\QuestionsAnswers\Block\Question;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Teqt\QuestionsAnswers\Api\QuestionRepositoryInterface;
use Teqt\QuestionsAnswers\Block\AbstractBlock;

class QuestionList extends AbstractBlock
{
    /**
     * @var ProductInterface
     */
    protected $product;

    /**
     * @var QuestionRepositoryInterface
     */
    protected $questionRepository;

    /**
     * Questions constructor.
     * @param Template\Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param QuestionRepositoryInterface $questionRepository
     * @param array $data
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function __construct(
        Template\Context $context,
        ProductRepositoryInterface $productRepository,
        QuestionRepositoryInterface $questionRepository,
        array $data = []
    )
    {
        $productId = $context->getRequest()->getParam('id');
        $this->product = $productRepository->getById($productId);
        $this->questionRepository = $questionRepository;

        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTitle()
    {
        return __('Questions from our community');
    }

    /**
     * @return \Teqt\QuestionsAnswers\Model\Resource\Question\Collection
     */
    public function getQuestions()
    {
        return $this->questionRepository
            ->findByProduct($this->product);
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }
}