<?php

namespace Teqt\QuestionsAnswers\Controller\Answer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Teqt\QuestionsAnswers\Api\AnswerRepositoryInterface;
use Teqt\QuestionsAnswers\Api\GuestRepositoryInterface;
use Teqt\QuestionsAnswers\Api\QuestionRepositoryInterface;
use Teqt\QuestionsAnswers\Controller\ContributionController;
use Teqt\QuestionsAnswers\Model\AnswerInterfaceFactory;
use Teqt\QuestionsAnswers\Model\GuestInterfaceFactory;

class Submit extends Action
{
    use ContributionController;

    /**
     * @var QuestionRepositoryInterface
     */
    protected $questionRepository;

    /**
     * @var GuestRepositoryInterface
     */
    protected $guestRepository;

    /**
     * @var GuestInterfaceFactory
     */
    protected $guestFactory;

    /**
     * @var AnswerRepositoryInterface
     */
    protected $answerRepository;

    /**
     * @var AnswerInterfaceFactory
     */
    protected $answerFactory;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * Submit constructor.
     * @param Context $context
     * @param QuestionRepositoryInterface $questionRepository
     * @param GuestRepositoryInterface $guestRepository
     * @param GuestInterfaceFactory $guestFactory
     * @param AnswerRepositoryInterface $answerRepository
     * @param AnswerInterfaceFactory $answerFactory
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        QuestionRepositoryInterface $questionRepository,
        GuestRepositoryInterface $guestRepository,
        GuestInterfaceFactory $guestFactory,
        AnswerRepositoryInterface $answerRepository,
        AnswerInterfaceFactory $answerFactory,
        ResultFactory $resultFactory
    )
    {
        $this->questionRepository = $questionRepository;
        $this->guestRepository = $guestRepository;
        $this->guestFactory = $guestFactory;
        $this->answerRepository = $answerRepository;
        $this->answerFactory = $answerFactory;
        $this->resultFactory = $resultFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        if(! $this->_isValid(['question_id', 'display_name', 'email', 'answer']))
        {
            return $this->_redirectReferer();
        }

        if(! ($question = $this->questionRepository->findById($this->getRequest()->getParam('question_id'))))
        {
            $this->messageManager->addErrorMessage(__("Given question not found."));
            return $this->_redirectReferer();
        }

        $guest = $this->_getQuest($this->getRequest()->getParams());
        $answer = $this->answerFactory->create([
            'answer' => $this->getRequest()->getParam('answer'),
            'question' => $question,
            'guest' => $guest
        ]);

        $this->answerRepository->save($answer);
        $this->messageManager->addSuccessMessage(__('Your answer was succesfully posted!'));
        return $this->_redirectReferer();
    }
}