<?php

namespace Teqt\QuestionsAnswers\Controller;

use Magento\Framework\Controller\ResultFactory;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;

trait ContributionController
{
    /**
     * @param array $requiredInput
     * @return bool
     */
    protected function _isValid(array $requiredInput)
    {
        foreach ($requiredInput as $key)
        {
            if($this->getRequest()->getParam($key, false))
            {
                continue;
            }

            $this->messageManager->addErrorMessage(__("One of the required attributes not given: %1.", $key));
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     * @return GuestInterface
     */
    protected function _getQuest(array $data)
    {
        $guest = $this->guestRepository
            ->findByEmail($data['email']);

        if ($guest)
        {
            return $guest;
        }

        $slug = strtolower(preg_replace('/[^a-z0-9]+/i', '-', $data['display_name']));

        $guest = $this->guestFactory->create([
            'display_name' => $data['display_name'],
            'email' => $data['email'],
            'slug' => $slug,
        ]);

        $this->guestRepository->save($guest);
        return $guest;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    protected function _redirectReferer()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $result->setUrl($this->_redirect->getRefererUrl());

        return $result;
    }
}