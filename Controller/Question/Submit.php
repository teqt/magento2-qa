<?php

namespace Teqt\QuestionsAnswers\Controller\Question;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Teqt\QuestionsAnswers\Api\GuestRepositoryInterface;
use Teqt\QuestionsAnswers\Api\QuestionRepositoryInterface;
use Teqt\QuestionsAnswers\Controller\ContributionController;
use Teqt\QuestionsAnswers\Model\GuestInterfaceFactory;
use Teqt\QuestionsAnswers\Model\QuestionInterfaceFactory;

class Submit extends Action
{
    use ContributionController;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var GuestRepositoryInterface
     */
    protected $guestRepository;

    /**
     * @var GuestInterfaceFactory
     */
    protected $guestFactory;

    /**
     * @var QuestionRepositoryInterface
     */
    protected $questionRepository;

    /**
     * @var QuestionInterfaceFactory
     */
    protected $questionFactory;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * Submit constructor.
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param GuestRepositoryInterface $guestRepository
     * @param GuestInterfaceFactory $guestFactory
     * @param QuestionRepositoryInterface $questionRepository
     * @param QuestionInterfaceFactory $questionFactory
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        GuestRepositoryInterface $guestRepository,
        GuestInterfaceFactory $guestFactory,
        QuestionRepositoryInterface $questionRepository,
        QuestionInterfaceFactory $questionFactory,
        ResultFactory $resultFactory
    )
    {
        $this->productRepository = $productRepository;
        $this->guestRepository = $guestRepository;
        $this->guestFactory = $guestFactory;
        $this->questionRepository = $questionRepository;
        $this->questionFactory = $questionFactory;
        $this->resultFactory = $resultFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        if(! $this->_isValid(['product_id', 'display_name', 'email', 'question']))
        {
            return $this->_redirectReferer();
        }

        if(! ($product = $this->productRepository->getById($this->getRequest()->getParam('product_id'))))
        {
            $this->messageManager->addErrorMessage(__("Given product not found."));
            return $this->_redirectReferer();
        }

        $guest = $this->_getQuest($this->getRequest()->getParams());
        $question = $this->questionFactory->create([
            'question' => $this->getRequest()->getParam('question'),
            'product' => $product,
            'guest' => $guest
        ]);

        $this->questionRepository->save($question);
        $this->messageManager->addSuccessMessage(__('Your question was succesfully posted!'));
        return $this->_redirectReferer();
    }
}