<?php

namespace Teqt\QuestionsAnswers\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;
use Teqt\QuestionsAnswers\Api\GuestRepositoryInterface;

class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    protected $actionFactory;

    /**
     * @var GuestRepositoryInterface
     */
    protected $guestRepository;

    /**
     * Router constructor.
     * @param \Magento\Framework\App\ActionFactory $actionFactory
     * @param GuestRepositoryInterface $guestRepository
     */
    public function __construct(ActionFactory $actionFactory, GuestRepositoryInterface $guestRepository)
    {
        $this->actionFactory = $actionFactory;
        $this->guestRepository = $guestRepository;
    }

    /**
     * Match application action by request
     * @param RequestInterface $request
     * @return ActionInterface
     */
    public function match(RequestInterface $request)
    {
        $url = trim($request->getPathInfo());
        $parts = explode('/', ltrim($url, '/'));

        // If front does not match, and no parts are left after shifting, we cannot handle this request
        if(array_shift($parts) !== 'qa' || ! count($parts))
        {
            return null;
        }

        $slug = array_shift($parts);
        $guest = $this->guestRepository->findBySlug($slug);
        if(! $guest->getId())
        {
            return null;
        }

        $request->setModuleName('qa')
            ->setControllerName('guest')
            ->setActionName('view')
            ->setParam('guest_id', $guest->getId());

        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $request->getPathInfo());
        return $this->actionFactory->create(Forward::class, [
            'request' => $request
        ]);
    }
}