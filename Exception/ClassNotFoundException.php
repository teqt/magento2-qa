<?php

namespace Teqt\QuestionsAnswers\Exception;

/**
 * Class ClassNotFoundException
 *
 * Since we shouldn't use the Zend Framework API in our Magento
 * plugin, introduce an own exception for missing or unconfigured
 * classes.
 *
 * @package Teqt\QuestionsAnswers\Exception
 */
class ClassNotFoundException extends \Exception
{
    /** @todo Implement some additional logic */
}