<?php

namespace Teqt\QuestionsAnswers\Exception;

/**
 * Class MissingArgumentException
 * @package Teqt\QuestionsAnswers\Exception
 */
class MissingArgumentException extends \Exception
{
    /** @todo Implement some additional logic */
}