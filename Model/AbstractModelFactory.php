<?php

namespace Teqt\QuestionsAnswers\Model;

use Teqt\QuestionsAnswers\AbstractFactory;
use Teqt\QuestionsAnswers\Exception\ClassNotFoundException;

abstract class AbstractModelFactory extends AbstractFactory
{
    /**
     * @param array $data
     * @return mixed
     * @throws ClassNotFoundException
     */
    public function create(array $data = [])
    {
        $model = parent::create();
        foreach ($data as $key => $value) {
            $model->setDataUsingMethod($key, $value);
        }

        return $model;
    }

    /**
     * @return string
     */
    abstract public function context();
}