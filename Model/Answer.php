<?php

namespace Teqt\QuestionsAnswers\Model;

use Magento\Framework\Model\AbstractModel;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Api\Data\AnswerInterface;
use Teqt\QuestionsAnswers\Api\Data\QuestionInterface;
use Teqt\QuestionsAnswers\Model\Resource\Answer as AnswerResource;

class Answer extends AbstractModel implements AnswerInterface
{
    use RelationalModel;

    /**
     * Initialize this model
     */
    protected function _construct()
    {
        $this->_init(AnswerResource::class);
    }

    /**
     * @return GuestInterface
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function getGuest()
    {
        return $this->_getParent(GuestInterface::class);
    }

    /**
     * @param GuestInterface $guest
     * @return $this
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function setGuest(GuestInterface $guest)
    {
        return $this->_setParent(GuestInterface::class, $guest);
    }

    /**
     * @return QuestionInterface
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function getQuestion()
    {
        return $this->_getParent(QuestionInterface::class);
    }

    /**
     * @param QuestionInterface $question
     * @return $this
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function setQuestion(QuestionInterface $question)
    {
        return $this->_setParent(QuestionInterface::class, $question);
    }
}