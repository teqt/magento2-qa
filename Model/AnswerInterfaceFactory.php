<?php

namespace Teqt\QuestionsAnswers\Model;

use Teqt\QuestionsAnswers\Api\Data\AnswerInterface;

class AnswerInterfaceFactory extends AbstractModelFactory
{
    /**
     * @return string
     */
    public function context()
    {
        return AnswerInterface::class;
    }
}