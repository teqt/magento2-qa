<?php

namespace Teqt\QuestionsAnswers\Model;

use Magento\Framework\Model\AbstractModel;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Model\Resource\Answer\Collection as AnswerCollection;
use Teqt\QuestionsAnswers\Model\Resource\Guest as GuestResource;
use Teqt\QuestionsAnswers\Model\Resource\Question\Collection as QuestionCollection;

class Guest extends AbstractModel implements GuestInterface
{
    use RelationalModel;

    /**
     * Initialize this model
     */
    protected function _construct()
    {
        $this->_init(GuestResource::class);
    }

    /**
     * @param string $displayField
     * @return string
     */
    public function getLink($displayField)
    {
        return sprintf('<a href="/qa/%s">%s</a>', $this->getSlug(),
            $this->getDataUsingMethod($displayField));
    }

    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $slug = strtolower(preg_replace('/[^0-9a-z]+/i', '-', $slug));
        return parent::setSlug($slug);
    }

    /**
     * @return QuestionCollection|void
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function getQuestions()
    {
        return $this->_getChildCollection(QuestionCollection::class)
            ->addGuestFilter($this);
    }

    /**
     * @return AnswerCollection|void
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function getAnswers()
    {
        return $this->_getChildCollection(AnswerCollection::class)
            ->addGuestFilter($this);
    }
}