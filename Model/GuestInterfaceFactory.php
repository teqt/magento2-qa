<?php

namespace Teqt\QuestionsAnswers\Model;

use Teqt\QuestionsAnswers\Api\Data\GuestInterface;

class GuestInterfaceFactory extends AbstractModelFactory
{
    /**
     * @return string
     */
    public function context()
    {
        return GuestInterface::class;
    }
}