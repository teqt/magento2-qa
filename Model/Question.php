<?php

namespace Teqt\QuestionsAnswers\Model;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Model\AbstractModel;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Api\Data\QuestionInterface;
use Teqt\QuestionsAnswers\Model\Resource\Question as QuestionResource;
use Teqt\QuestionsAnswers\Model\Resource\Answer\Collection as AnswerCollection;

class Question extends AbstractModel implements QuestionInterface
{
    use RelationalModel;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(QuestionResource::class);
    }

    /**
     * @return GuestInterface
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function getGuest()
    {
        return $this->_getParent(GuestInterface::class);
    }

    /**
     * @param GuestInterface $guest
     * @return $this
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function setGuest(GuestInterface $guest)
    {
        return $this->_setParent(GuestInterface::class, $guest);
    }

    /**
     * @return ProductInterface|mixed
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function getProduct()
    {
        return $this->_getParent(ProductInterface::class);
    }

    /**
     * @param ProductInterface $product
     * @return $this
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function setProduct(ProductInterface $product)
    {
        return $this->_setParent(ProductInterface::class, $product);
    }

    /**
     * @return AnswerCollection|void
     * @throws \Teqt\QuestionsAnswers\Exception\MissingArgumentException
     */
    public function getAnswers()
    {
        return $this->_getChildCollection(AnswerCollection::class)
            ->addQuestionFilter($this);
    }
}