<?php

namespace Teqt\QuestionsAnswers\Model;

use Teqt\QuestionsAnswers\Api\Data\QuestionInterface;

class QuestionInterfaceFactory extends AbstractModelFactory
{
    /**
     * @return string
     */
    public function context()
    {
        return QuestionInterface::class;
    }
}