<?php

namespace Teqt\QuestionsAnswers\Model;

use Magento\Framework\App\ObjectManager;
use Teqt\QuestionsAnswers\Exception\MissingArgumentException;

/**
 * Interface to define some constants in
 * @package Teqt\QuestionsAnswers\Model
 */
interface RelationalModelDefinitions
{
    /**
     * Regex to substract context from interface
     */
    const CONTEXT_INTERFACE_REGEX = '/\\\([^\\\]+)Interface$/';

    /**
     * Regex to substract context from collection
     */
    const CONTEXT_COLLECTION_REGEX = '/\\\([^\\\]+)\\\Collection$/';
}

/**
 * Trait RelationalModel
 *
 * Generic code to deal with relations between models
 *
 * @package Teqt\QuestionsAnswers\Model
 */
trait RelationalModel
{
    /**
     * @param $className
     * @param null $context
     * @return mixed
     * @throws MissingArgumentException
     */
    protected function _getParent($className, $context = null)
    {
        $context = $this->_getParentContextFromClassName($className, $context);
        $identifier = $context . '_id';

        if($this->hasData($context))
        {
            return $this->getData($context);
        }

        $model = ObjectManager::getInstance()
            ->create($className);
        if($this->hasData($identifier))
        {
            $model->load((int) $this->getData($identifier));
            $this->setData($context, $model);
        }

        return $this->getData($context);
    }

    /**
     * @param $className
     * @param \Magento\Framework\Model\AbstractModel $model
     * @param null $context
     * @return $this
     * @throws MissingArgumentException
     */
    protected function _setParent($className, \Magento\Framework\Model\AbstractModel $model, $context = null)
    {
        $context = $this->_getParentContextFromClassName($className, $context);
        $identifier = $context . '_id';

        $this->setData($context, $model);
        $this->setData($identifier, $model->getId());

        return $this;
    }

    /**
     * @param $className Collection to retrieve
     * @param null $context
     * @return mixed
     * @throws MissingArgumentException
     */
    protected function _getChildCollection($className)
    {
        $context = $this->_getChildContextFromClassName($className);
        if($this->hasData($context))
        {
            return $this->getData($context);
        }

        $collection = ObjectManager::getInstance()
            ->create($className);
        $this->setData($context, $collection);
        return $this->getData($context);
    }

    /**
     * @param $className
     * @param null $context
     * @return null|string
     * @throws MissingArgumentException
     */
    private function _getParentContextFromClassName($className, $context = null)
    {
        return $this->_getContextFromClassName($className, RelationalModelDefinitions::CONTEXT_INTERFACE_REGEX, $context);
    }

    /**
     * @param $className
     * @param null $context
     * @return null|string
     * @throws MissingArgumentException
     */
    private function _getChildContextFromClassName($className, $context = null)
    {
        return $this->_getContextFromClassName($className, RelationalModelDefinitions::CONTEXT_COLLECTION_REGEX, $context);
    }

    /**
     * @param $className Context to look into
     * @param $regex Regex to search with
     * @param null $context Given context
     * @return string Underscored context from class name
     * @throws MissingArgumentException
     */
    private function _getContextFromClassName($className, $regex, $context = null)
    {
        if(! is_null($context))
        {
            return $context;
        }

        if(array_key_exists($className, self::$contextCache))
        {
            return self::$contextCache[$className];
        }

        if(! preg_match($regex, $className, $match))
        {
            throw new MissingArgumentException(sprintf("Could not extract context from className '%s'. You need to set one manually.", $className));
        }

        self::$contextCache[$className] = $this->_underscore($match[1]);
        return self::$contextCache[$className];
    }

    /**
     * @var array
     */
    static protected $contextCache = [];
}