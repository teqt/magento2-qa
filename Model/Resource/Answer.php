<?php

namespace Teqt\QuestionsAnswers\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Answer extends AbstractDb
{
    /**
     * Construct Answer resource
     */
    public function _construct()
    {
        $this->_init('teqt_answer', 'id');
    }
}