<?php

namespace Teqt\QuestionsAnswers\Model\Resource\Answer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Teqt\QuestionsAnswers\Model\Answer;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Api\Data\QuestionInterface;
use Teqt\QuestionsAnswers\Model\Resource\Answer as AnswerResource;

class Collection extends AbstractCollection
{
    /**
     * Define answer collection
     */
    public function _construct()
    {
        $this->_init(Answer::class, AnswerResource::class);
    }

    /**
     * @param GuestInterface $guest
     * @return $this
     */
    public function addGuestFilter(GuestInterface $guest)
    {
        $this->getSelect()->where('guest_id = ?', $guest->getId());
        return $this;
    }

    /**
     * @param QuestionInterface $question
     * @return $this
     */
    public function addQuestionFilter(QuestionInterface $question)
    {
        $this->getSelect()->where('question_id = ?', $question->getId());
        return $this;
    }
}