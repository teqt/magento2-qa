<?php

namespace Teqt\QuestionsAnswers\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Guest extends AbstractDb
{
    /**
     * Construct Guest resource
     */
    public function _construct()
    {
        $this->_init('teqt_guest', 'id');
    }
}