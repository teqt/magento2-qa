<?php

namespace Teqt\QuestionsAnswers\Model\Resource\Guest;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Teqt\QuestionsAnswers\Model\Guest;
use Teqt\QuestionsAnswers\Model\Resource\Guest as GuestResource;

class Collection extends AbstractCollection
{
    /**
     * Define answer collection
     */
    public function _construct()
    {
        $this->_init(Guest::class, GuestResource::class);
    }
}