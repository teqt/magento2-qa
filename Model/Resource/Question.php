<?php

namespace Teqt\QuestionsAnswers\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Question extends AbstractDb
{
    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('teqt_question', 'id');
    }
}