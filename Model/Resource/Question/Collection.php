<?php

namespace Teqt\QuestionsAnswers\Model\Resource\Question;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Model\Question;
use Teqt\QuestionsAnswers\Model\Resource\Question as QuestionResource;

class Collection extends AbstractCollection
{
    /**
     * Define question collection
     */
    protected function _construct()
    {
        $this->_init(Question::class, QuestionResource::class);
    }

    /**
     * @param GuestInterface $guest
     * @return $this
     */
    public function addGuestFilter(GuestInterface $guest)
    {
        $this->getSelect()->where('guest_id = ?', $guest->getId());
        return $this;
    }

    /**
     * @param ProductInterface $product
     * @return $this
     */
    public function addProductFilter(ProductInterface $product)
    {
        $this->getSelect()->where('product_id = ?', $product->getId());
        return $this;
    }
}