<?php

namespace Teqt\QuestionsAnswers\Model\Resource\Question;

use Teqt\QuestionsAnswers\AbstractFactory;

class CollectionFactory extends AbstractFactory
{
    /**
     * @return string
     */
    public function context()
    {
        return Collection::class;
    }
}