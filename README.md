# Magento 2 example code

## Before we dive into the code.

This project is a plugin to give an insight in my knowledge of Magento2. With over 7 years of experience in Magento 1, I used this plugin to transform my knowledge from Magento 1 to Magento 2:

- What structure to expect, and how plugins are registered
- Whether routing is changed, and how to implement custom routers the new way
- How to properly implement custom models and collections
- How to override / append to existing code / templates

My environment: Magento CE 2.3.0, with sample data installed.

### My plan of action

Since a real-life example works best for creating example code, I choose to create a plugin facilitating Q&A functionality for product pages. I started out real basic, without much validation or moderation tools. You should not use this plugin in production, let alone without consulting a Magento developer.

I have to add actual functionality to the product page, but still, any feedback is welcome! Don't hestitate to contact me, at teqt@teqt.nl.

## Getting Started

These instructions will install this plugin into your Magento2 installation.

### Installing

Just install through composer:

```shell
~# composer config repositories.magento2-qa vcs git@bitbucket.org:teqt/magento2-qa.git
~# composer require teqt/magento2-qa
```

Enable this module:

```shell
~# ./bin/magento module:enable Teqt_QuestionsAnswers
```

Clear Magento cache and upgrade your setup:

```shell
~# ./bin/magento cache:flush
~# ./bin/magento setup:upgrade
```

### End result

You should now have the database model installed, and see product pages have an extra tab with consumer questions. Have fun!

## Acknowledgments

* Learned Magento's new code conventions and best practices
* Learned the basics for Magento plugin development: dependency injection, routing, databases, templating and packaging.
* Just had a fun and challenging task to work on.
