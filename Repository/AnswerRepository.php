<?php

namespace Teqt\QuestionsAnswers\Repository;

use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Teqt\QuestionsAnswers\Api\Data\AnswerInterface;
use Teqt\QuestionsAnswers\Api\AnswerRepositoryInterface;
use Teqt\QuestionsAnswers\Model\AnswerInterfaceFactory;
use Teqt\QuestionsAnswers\Model\Resource\Answer as AnswerResource;

class AnswerRepository extends AbstractRepository implements AnswerRepositoryInterface
{
    /**
     * AnswerRepository constructor.
     * @param AnswerResource $resource
     * @param AnswerResource\CollectionFactory $collectionFactory
     * @param CollectionProcessor $collectionProcessor
     * @param AnswerInterfaceFactory $factory
     */
    public function __construct(
        AnswerResource $resource,
        AnswerResource\CollectionFactory $collectionFactory,
        CollectionProcessor $collectionProcessor,
        AnswerInterfaceFactory $factory
    )
    {
        $args = func_get_args();
        parent::__construct(...$args);
    }

    /**
     * @param AnswerInterface $answer
     * @throws \Exception
     */
    public function save(AnswerInterface $answer)
    {
        $this->_save($answer);
    }
}