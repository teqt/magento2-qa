<?php

namespace Teqt\QuestionsAnswers\Repository;

use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Magento\Framework\App\RequestInterface;
use Teqt\QuestionsAnswers\Exception\MissingArgumentException;
use Teqt\QuestionsAnswers\Api\Data\GuestInterface;
use Teqt\QuestionsAnswers\Api\GuestRepositoryInterface;
use Teqt\QuestionsAnswers\Model\GuestInterfaceFactory;
use Teqt\QuestionsAnswers\Model\Resource\Guest as GuestResource;

/**
 * Class QuestionRepository
 * @package Teqt\QuestionsAnswers\Model
 */
class GuestRepository extends AbstractRepository implements GuestRepositoryInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * QuestionRepository constructor.
     * @param GuestResource $resource
     * @param GuestResource\CollectionFactory $collectionFactory
     * @param CollectionProcessor $collectionProcessor
     * @param GuestInterfaceFactory $factory
     * @param RequestInterface $request
     */
    public function __construct(
        GuestResource $resource,
        GuestResource\CollectionFactory $collectionFactory,
        CollectionProcessor $collectionProcessor,
        GuestInterfaceFactory $factory,
        RequestInterface $request
    )
    {
        $args = func_get_args();
        $this->request = array_pop($args);
        parent::__construct(...$args);
    }

    /**
     * @param string $slug
     * @return GuestInterface
     * @throws \Teqt\QuestionsAnswers\Exception\ClassNotFoundException
     */
    public function findBySlug($slug)
    {
        return $this->_findBy($slug, 'slug');
    }

    /**
     * @param string $email
     * @return GuestInterface
     * @throws \Teqt\QuestionsAnswers\Exception\ClassNotFoundException
     */
    public function findByEmail($email)
    {
        return $this->_findBy($email, 'email');
    }

    /**
     * @return \Magento\Framework\Model\AbstractModel|GuestInterface
     * @throws MissingArgumentException
     * @throws \Teqt\QuestionsAnswers\Exception\ClassNotFoundException
     */
    public function findByRequest()
    {
        if(! ($guestId = $this->request->getParam('guest_id', false)))
        {
            throw new MissingArgumentException("Guest id is not set in current request.");
        }

        return $this->findById($guestId);
    }

    /**
     * @param GuestInterface $question
     * @throws \Exception
     */
    public function save(GuestInterface $question)
    {
        $this->_save($question);
    }
}