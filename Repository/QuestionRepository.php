<?php

namespace Teqt\QuestionsAnswers\Repository;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Teqt\QuestionsAnswers\Api\Data\QuestionInterface;
use Teqt\QuestionsAnswers\Api\QuestionRepositoryInterface;
use Teqt\QuestionsAnswers\Model\QuestionInterfaceFactory;
use Teqt\QuestionsAnswers\Model\Resource\Question as QuestionResource;
use Teqt\QuestionsAnswers\Model\Resource\Question\Collection;

/**
 * Class QuestionRepository
 * @package Teqt\QuestionsAnswers\Model
 */
class QuestionRepository extends AbstractRepository implements QuestionRepositoryInterface
{
    /**
     * QuestionRepository constructor.
     * @param QuestionResource $resource
     * @param QuestionResource\CollectionFactory $collectionFactory
     * @param CollectionProcessor $collectionProcessor
     * @param QuestionInterfaceFactory $factory
     */
    public function __construct(
        QuestionResource $resource,
        QuestionResource\CollectionFactory $collectionFactory,
        CollectionProcessor $collectionProcessor,
        QuestionInterfaceFactory $factory
    )
    {
        $args = func_get_args();
        parent::__construct(...$args);
    }

    /**
     * @param ProductInterface $product
     * @return Collection
     * @throws \Teqt\QuestionsAnswers\Exception\ClassNotFoundException
     */
    public function findByProduct($product)
    {
        return $this->collectionFactory->create()
            ->addProductFilter($product);
    }

    /**
     * @param QuestionInterface $question
     * @throws \Exception
     */
    public function save(QuestionInterface $question)
    {
        $this->_save($question);
    }
}