<?php

namespace Teqt\QuestionsAnswers\Setup;

use Magento\Backend\Block\Widget\Tab;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Install required Database schema for this table
 * @package Teqt\QuestionsAnswers\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install required tables for this module
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tableName = $setup->getTable('teqt_guest');
        if( $setup->getConnection()->isTableExists($tableName) !== true)
        {
            $definition = $this->createGuestTableDefinition($setup, $tableName);
            $setup->getConnection()->createTable($definition);
        }

        $tableName = $setup->getTable('teqt_question');
        if( $setup->getConnection()->isTableExists($tableName) !== true)
        {
            $definition = $this->createQuestionTableDefinition($setup, $tableName);
            $setup->getConnection()->createTable($definition);
        }

        $tableName = $setup->getTable('teqt_answer');
        if( $setup->getConnection()->isTableExists($tableName) !== true)
        {
            $definition = $this->createAnswerTableDefinition($setup, $tableName);
            $setup->getConnection()->createTable($definition);
        }

        $setup->endSetup();
    }

    /**
     * Create guest table definition for setup
     * @param SchemaSetupInterface $setup
     * @param string $tableName
     * @return Table
     * @throws \InvalidArgumentException
     * @throws \Zend_Db_Exception
     */
    protected function createGuestTableDefinition(SchemaSetupInterface $setup, $tableName)
    {
        if( ! is_string($tableName))
        {
            throw new \InvalidArgumentException( "Parameter \$tableName should be a string." );
        }

        return $setup->getConnection()
            ->newTable($tableName)
            ->addColumn('id', Table::TYPE_INTEGER, null, [
                TABLE::OPTION_IDENTITY  => true,
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_PRIMARY   => true,
                TABLE::OPTION_NULLABLE  => false,
            ])
            ->addColumn('slug', Table::TYPE_TEXT, 32, [
                TABLE::OPTION_NULLABLE  => false
            ])
            ->addColumn('display_name', Table::TYPE_TEXT, 255, [
                TABLE::OPTION_NULLABLE  => false
            ])
            ->addColumn('email', Table::TYPE_TEXT, 255, [
                TABLE::OPTION_NULLABLE  => true
            ])
        ;
    }

    /**
     * Create question table definition for setup
     * @param SchemaSetupInterface $setup
     * @param string $tableName
     * @return Table
     * @throws \InvalidArgumentException
     * @throws \Zend_Db_Exception
     */
    protected function createQuestionTableDefinition(SchemaSetupInterface $setup, $tableName)
    {
        if( ! is_string($tableName))
        {
            throw new \InvalidArgumentException( "Parameter \$tableName should be a string." );
        }

        $guestTable = $setup->getTable('teqt_guest');
        $productTable = $setup->getTable('catalog_product_entity');
        return $setup->getConnection()
            ->newTable($tableName)
            ->addColumn('id', Table::TYPE_INTEGER, null, [
                TABLE::OPTION_IDENTITY  => true,
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_PRIMARY   => true,
                TABLE::OPTION_NULLABLE  => false,
            ])
            ->addColumn('question', Table::TYPE_TEXT, 255, [
                TABLE::OPTION_NULLABLE  => false,
                TABLE::OPTION_DEFAULT   => ''
            ])
            ->addColumn('product_id', Table::TYPE_INTEGER, null, [
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_NULLABLE  => true
            ])
            ->addColumn('guest_id', Table::TYPE_INTEGER, null, [
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_NULLABLE  => true
            ])
            ->addForeignKey(
                $setup->getFkName($tableName, 'product_id', $productTable, 'id'),
                'product_id', $productTable, 'entity_id',
                Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $setup->getFkName($tableName, 'guest_id', $guestTable, 'id'),
                'guest_id', $guestTable, 'id',
                Table::ACTION_SET_NULL
            )
        ;
    }

    /**
     * Create answer table definition for setup
     * @param SchemaSetupInterface $setup
     * @param string $tableName
     * @return Table
     * @throws \InvalidArgumentException
     * @throws \Zend_Db_Exception
     */
    protected function createAnswerTableDefinition(SchemaSetupInterface $setup, $tableName)
    {
        if( ! is_string($tableName))
        {
            throw new \InvalidArgumentException( "Parameter \$tableName should be a string." );
        }

        $guestTable = $setup->getTable('teqt_guest');
        $questionTable = $setup->getTable('teqt_question');
        return $setup->getConnection()
            ->newTable($tableName)
            ->addColumn('id', Table::TYPE_INTEGER, null, [
                TABLE::OPTION_IDENTITY  => true,
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_PRIMARY   => true,
                TABLE::OPTION_NULLABLE  => false,
            ])
            ->addColumn('question_id', Table::TYPE_INTEGER, null, [
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_NULLABLE  => false
            ])
            ->addColumn('answer', Table::TYPE_TEXT, 255, [
                TABLE::OPTION_NULLABLE  => false,
                TABLE::OPTION_DEFAULT   => ''
            ])
            ->addColumn('guest_id', Table::TYPE_INTEGER, null, [
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_NULLABLE  => true
            ])
            ->addForeignKey(
                $setup->getFkName($tableName, 'question_id', $questionTable, 'id'),
                'question_id', $questionTable, 'id',
                Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $setup->getFkName($tableName, 'guest_id', $guestTable, 'id'),
                'guest_id', $guestTable, 'id',
                Table::ACTION_SET_NULL
            )
        ;
    }
}